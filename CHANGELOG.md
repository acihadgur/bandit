# Bandit analyzer changelog

## v2.12.4
- Update `bandit` to [v1.7.4](https://pypi.org/project/bandit/1.7.4/) (CHANGELOG)[https://github.com/PyCQA/bandit/releases/tag/1.7.4] (!90)
- Update direct Go modules (includes indirect dependencies):
    - `analyzers/command` to `v1.6.1`
    - `analyzers/report` to `v3.10.0`

## v2.12.3
- Update bandit to [v1.7.2](https://pypi.org/project/bandit/1.7.2/) (!86)
    - Fixes [broken URL for information about Bandit rule B107](https://github.com/PyCQA/bandit/pull/751)
    - Adds `snmp_security_check` plugin for SNMP version and cryptography
- Update Go dependencies:
    - go-cmp to v0.5.7
    - analyzers/command to v1.6.0
    - analyzers/report to v3.8.0
    - analyzers/ruleset to v1.4.0
    - x/crypto to v0.0.0-20220131195533-30dcbda58838
- Upgrade Alpine dependencies to resolve:
    - CVE-2022-23852 and CVE-2022-23990 in `expat-2.4.3-r0`
    - CVE-2021-3995 and CVE-2021-3996 in `libuuid-2.37.2-r1`

## v2.12.2
- Update bandit to [v1.7.1](https://pypi.org/project/bandit/1.7.1/) (!85)
- Update logrus golang dependency to v1.8.1
- Update command golang dependency to v1.5.1
- Update common golang dependency to v2.24.1
- Update report golang dependency to v3.7.1
- Update ruleset golang dependency to v1.3.0

## v2.12.1
- Update go to v1.17 (!84)

## v2.12.0
- Update report dependency in order to use the report schema version 14.0.0 (!68)

## v2.11.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!66)

## v2.11.0
- Update bandit version to 1.7.0 (!65)

## v2.10.1
- Update bandit version to 1.6.3 (!62 @thomas-nilsson-irfu)
- Drop support for Python 2 (!62 @thomas-nilsson-irfu)

## v2.10.0
- Update common to v2.22.0 (!60)
- Update urfave/cli to v2.3.0 (!60)

## v2.9.6
- Update common to version v2.21.3 (!59)
-
## v2.9.5
- Update common and enable disablement of custom rulesets (!58)

## v2.9.4
- Update golang dependencies (!57)

## v2.9.3
- Update finding links to new location (!52)

## v2.9.2
- Update golang dependencies (!50)

## v2.9.1
- Update golang dependencies (!48)
- Add documentation about which Python version to use (!48)

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!46)

## v2.8.0
- Switch base image to alpine (!45)

## v2.7.2
- Upgrade go to version 1.15 (!44)

## v2.7.1
- Log error when failing to create a code fingerprint (!43)

## v2.7.0
- Add scan object to report (!40)

## v2.6.0
- Switch to the MIT Expat license (!37)

## v2.5.0
- Update logging to be standardized across analyzers (!36)

## v2.4.1
- Use a Slim base image (!28)
- Remove `location.dependency` from the generated SAST report (!33)

## v2.4.0
- Add `id` field to vulnerabilities in JSON report (!20)

## v2.3.0
- Add support for custom CA certs (!17)

## v2.2.2
- Use Debian Stretch as base image (!14)

## v2.2.1
- Update bandit version to 1.6.2

## v2.2.0
- Add `SAST_BANDIT_EXCLUDED_PATHS` option to exclude paths from scan
- Add `SAST_EXCLUDED_PATHS` option to exclude paths from report (common v2.3.0)
- Report "confirmed" confidence instead of "critical" (common v2.2.0)

## v2.1.1
- Update common to v2.1.6

## v2.1.0
- Update bandit version to 1.5.1

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Enrich report with more data

## v1.0.0
- initial release
